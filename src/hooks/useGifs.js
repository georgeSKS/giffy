import { useState, useEffect, useContext } from "react";
import getGifs from "../services/getGifs";
import GifsContext from "../context/GifsContext";

const useGifs = ({ keyword } = { keyword: null }) => {
  // const [gifs, setGifs] = useState([]);
  const { gifs, setGifs } = useContext(GifsContext);
  const [loading, setLoading] = useState(false);

  useEffect(
    function () {
      setLoading(true);

      const keywordToUse =
        keyword || localStorage.getItem("lastKeyword") || "random";

      getGifs({ keyword: keywordToUse }).then((gifs) => {
        setGifs(gifs);
        setLoading(false);
        // guardamos la keyword en el localstorage
        localStorage.setItem("lastKeyword", keyword);
      });
    },
    [keyword]
  );

  return { loading, gifs };
};

export default useGifs;
