import React from "react";
import "./App.css";
import Home from "./pages/Home";
import Detail from "./pages/Detail";
import SearchResults from "./pages/SearchResults";
import StaticContext from "./context/StaticContext";
import { GifsContextProvider } from "./context/GifsContext";

import { Route, Link } from "wouter";

const initialState = {
  name: "jorge",
  value: true,
};

function App() {
  return (
    <StaticContext.Provider value={initialState}>
      <div className="App">
        <section className="App-content">
          <Link to="/">
            <h3>Gifs</h3>
          </Link>
          <GifsContextProvider>
            <Route component={Home} path="/" />
            <Route component={Detail} path="/gif/:id" />
            <Route component={SearchResults} path="/search/:keyword" />
          </GifsContextProvider>
        </section>
      </div>
    </StaticContext.Provider>
  );
}

export default App;
