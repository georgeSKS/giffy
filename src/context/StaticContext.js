import React from "react";

const Context = React.createContext({
  name: "esto-sin-provider",
  value: true,
});

export default Context;
