import React from "react";
import ListOfGifs from "components/ListOfGifs/";
import useGifs from "hooks/useGifs";

const SearchResults = ({ params }) => {
  const { keyword } = params;
  const { loading, gifs } = useGifs({ keyword });

  return <div>{loading ? "Loading ..." : <ListOfGifs gifs={gifs} />}</div>;
};

export default SearchResults;
