import React, { useContext } from "react";
import GifsContext from "context/GifsContext";
import Gif from "components/Gif";

const Detail = ({ params }) => {
  const { id } = params;
  const { gifs } = useContext(GifsContext);

  const gif = gifs.find((singleGif) => singleGif.id === id);

  return (
    <div>
      <Gif {...gif} />
    </div>
  );
};

export default Detail;
