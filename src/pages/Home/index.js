import React, { useState } from "react";
import { Link, useLocation } from "wouter";
import useGifs from "hooks/useGifs";
import ListOfGifs from "components/ListOfGifs";

const Home = () => {
  const [keyword, setKeyword] = useState("");
  const [path, pushLocation] = useLocation();
  const { loading, gifs } = useGifs();

  const handleSubmit = (evt) => {
    evt.preventDefault();
    pushLocation(`/search/${keyword}`);
  };

  const handleChange = (evt) => {
    setKeyword(evt.target.value);
  };

  return (
    <div>
      <form onSubmit={handleSubmit}>
        <input
          type="text"
          value={keyword}
          onChange={handleChange}
          placeholder="Search a gif here ..."
        />
      </form>
      <h3>Última busqueda</h3>
      <ListOfGifs gifs={gifs} />
      <h3>Los gifs más populares</h3>
      <div>
        <Link to="/search/mexico">Gifs de México</Link>
      </div>
      <div>
        <Link to="/search/matrix">Gifs de Matrix</Link>
      </div>
      <div>
        <Link to="/search/panda">Gifs de Pandas</Link>
      </div>
    </div>
  );
};

export default Home;
