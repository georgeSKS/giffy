import React, { useState, useEffect } from "react";
import Gif from "components/Gif";
import "./styles.css";

const ListOfGifs = ({ gifs }) => {
  return (
    <div className="ListOgGifs">
      {gifs.map((singleGif) => (
        <Gif
          url={singleGif.url}
          title={singleGif.title}
          id={singleGif.id}
          key={singleGif.id}
        />
      ))}
    </div>
  );
};

export default ListOfGifs;
