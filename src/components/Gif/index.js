import React from "react";
import "./styles.css";

import { Link } from "wouter";

const Gif = ({ id, title, url }) => {
  return (
    <div className="Gif">
      <Link to={`/gif/${id}`} className="Gif-link">
        <h4>{title}</h4>
        <img loading="lazy" alt={title} src={url} />
      </Link>
    </div>
  );
};

export default Gif;
